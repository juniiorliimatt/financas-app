import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaFinancasComponent } from './lista-financas/lista-financas.component';
import { NovaFinancaComponent } from './nova-financa/nova-financa.component';
import { EditarFinancaComponent } from './editar-financa/editar-financa.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaFinancasComponent,
    NovaFinancaComponent,
    EditarFinancaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
