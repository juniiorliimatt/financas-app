import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarFinancaComponent } from './editar-financa.component';

describe('EditarFinancaComponent', () => {
  let component: EditarFinancaComponent;
  let fixture: ComponentFixture<EditarFinancaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarFinancaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarFinancaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
